import { Routes, Route, useNavigate } from "react-router-dom";
import Header from "../components/Header";
import Home from "../page/Home";
import About from "../page/About";

function Layout() {
  const navigate = useNavigate();
  return (
    <div className="App">
      <Header />
      <div className="menu">
        {[
          { name: "Trang Chủ", route: "/" },
          { name: "Giới Thiệu", route: "/about" },
          { name: "Sản Phẩm", route: "/products" },
          { name: "Bài Viết", route: "/blogs" },
          { name: "Liên Hệ", route: "/contact" },
          { name: "Tuyển Dụng", route: "/career" },
        ].map((el, index) => (
          <div
            className="menu__item"
            onClick={() => navigate(el.route)}
            key={index}
          >
            {el.name}
          </div>
        ))}
      </div>

      {/* <div>{location}</div> */}

      <div className="body">
        <Routes>
          <Route index element={<Home />} />
          <Route path="/about" element={<About />} />
        </Routes>
      </div>
    </div>
  );
}

export default Layout;
