import "./App.css";
import Layout from "./layout";
import "./assets/css/index.scss";
import { Routes, Route, BrowserRouter } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="/home" element={<Layout />} />
            <Route path="/about" element={<Layout />} />
            <Route path="/products" element={<Layout />} />
            <Route path="/blogs" element={<Layout />} />
            <Route path="/contact" element={<Layout />} />
            <Route path="/career" element={<Layout />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
