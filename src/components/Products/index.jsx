function Products(props) {
  return (
    <div className="product">
      <img className="product__image" src={props.img} alt="" />
      <div className="product__name">{props.name}</div>
      <div className="product__price">{props.price}</div>
    </div>
  );
}

export default Products;
