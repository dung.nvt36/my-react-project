const Header = (props) => {
  return (
    <div className="header">
      <div className="header__logo">
        <img
          src="//theme.hstatic.net/200000459373/1000812576/14/logo.png?v=282"
          className="app-logo"
          alt="logo"
        />
      </div>
      <div className="header__navbar">
        <div className="address">
          <span>Giao hoặc đến lấy tại</span>
          <span className="header__navbar__item">
            <i className="fa-solid fa-chevron-down"></i>
          </span>
        </div>
        <div className="header__navbar__nav">
          <div className="header__navbar__nav__icon search">
            <i className="fa-solid fa-magnifying-glass"></i>
          </div>
          <div className="header__navbar__nav__icon user">
            <i className="fa-solid fa-user"></i>
          </div>
          <div className="header__navbar__nav__icon cart">
            <i className="fa-solid fa-cart-shopping"></i>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
