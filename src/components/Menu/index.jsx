export const Menu = (props) => {
  return (
    <div className="menu">
      <div className="menu__item">Trang chủ</div>
      <div className="menu__item">Giới thiệu</div>
      <div className="menu__item">Sản phẩm</div>
      <div className="menu__item">Bài viết</div>
      <div className="menu__item">Liên hệ</div>
      <div className="menu__item">Tuyển dụng</div>
    </div>
  );
};
