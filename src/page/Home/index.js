import Products from "../../components/Products";

function Home() {
  return (
    <div className="home">
      <div className="products">
        {[
          { name: "Cá hú tươi 500g", price: "32,800₫", img: "/ca-hu.jpg" },
          {
            name: "Canh bí đao thịt xay 300g",
            price: "30,000₫",
            img: "/canh-bi-dao.webp",
          },
          {
            name: "Canh bí đỏ tròn thịt xay 300g",
            price: "29,000₫",
            img: "/canh-bi-do.webp",
          },
          {
            name: "Canh bí đỏ tròn vỉ 300g",
            price: "16,000₫",
            img: "/canh-bi-do.webp",
          },
          {
            name: "Cánh gà 500g- Sanha",
            price: "20,000₫",
            img: "/canh-ga.webp",
          },
          {
            name: "Canh khổ qua bào vỉ 300g",
            price: "20,000₫",
            img: "/canh-kho-qua.webp",
          },
        ].map((el, index) => (
          <Products name={el.name} price={el.price} img={el.img} />
        ))}
      </div>
    </div>
  );
}

export default Home;
